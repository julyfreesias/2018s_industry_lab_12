package ictgradschool.industry.bounce;

import java.awt.*;

public class DynamicRectangleShape extends Shape {

    private boolean isfilled;
    private Color color;

    public DynamicRectangleShape() {
        super();
        this.color = Color.black;
    }
    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
        this.color = Color.black;
    }
    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, Color color) {
        super(x, y, deltaX, deltaY);
        this.color = color;
    }
    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color color) {
        super(x,y,deltaX,deltaY,width,height);
        this.color = color;
    }

    @Override
    public void move(int width, int height) {

        int oldX = fDeltaX;
        int oldY = fDeltaY;

        super.move(width, height);


        int newX =fDeltaX;
        int newY =fDeltaY;

        if(oldX!=newX){
            isfilled=true;
        }else if(oldY!=newY){
            isfilled=false;
        }

    }

    @Override
    public void paint(Painter painter) {

        if(isfilled){
            painter.setColor(color);
            painter.fillRect(fX,fY,fWidth,fHeight);
            painter.setColor(Color.black);
        }else{
            painter.setColor(Color.black);
            painter.drawRect(fX,fY,fWidth,fHeight);
        }

    }
}
