package ictgradschool.industry.bounce;

import java.awt.*;

public class GemShape extends Shape {
    public GemShape() {
        super();
    }


    /**
     * Creates a RectangleShape instance with specified values for instance
     * variables.
     *
     * @param x      x position.
     * @param y      y position.
     * @param deltaX speed and direction for horizontal axis.
     * @param deltaY speed and direction for vertical axis.
     */
    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }


    /**
     * Creates a RectangleShape instance with specified values for instance
     * variables.
     *
     * @param x      x position.
     * @param y      y position.
     * @param deltaX speed (pixels per move call) and direction for horizontal
     *               axis.
     * @param deltaY speed (pixels per move call) and direction for vertical
     *               axis.
     * @param width  width in pixels.
     * @param height height in pixels.
     */
    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }


    @Override
    public void paint(Painter painter) {

        Polygon polygon = null;

        if (fWidth > 40) {
            int[] xcord = new int[]{fX + 20,fX + fWidth - 20,fX + fWidth,fX + fWidth - 20,fX + 20,fX};
            int[] ycord = new int[]{fY, fY, fY + fHeight/2, fY + fHeight, fY + fHeight,fY + fHeight / 2};
            polygon = new Polygon(xcord, ycord, 6);
        } else if (fWidth <= 40) {
            int[] xcord = new int[]{fX + fWidth / 2, fX+ fWidth, fX + fWidth / 2, fX};
            int[] ycord = new int[]{fY, fY + fHeight / 2, fY+fHeight, fY + fHeight / 2};
            polygon = new Polygon(xcord, ycord, 4);
        }


        painter.drawPolygon(polygon);


    }
}
